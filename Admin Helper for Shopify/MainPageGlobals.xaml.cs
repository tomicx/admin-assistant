﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Core;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.Storage;
using System.Diagnostics;
using Windows.UI;

namespace Admin_Helper_for_Shopify
{

    public sealed partial class MainPage : Page
    {
        
        public MainPage()
        {
            this.InitializeComponent();
            webView.Visibility = Visibility.Collapsed;
            Uri Home = new Uri("https://www.shopify.com.au/signup"); 
            webView.Navigate(Home);

            DataContext = this;

            Debug.WriteLine("Alpha Bravo Charlie");

            Global.HTTPS = "https://";
            ShopUrlTwo.Text = "myshopify.com";
            ReadDataUrlContainer();
            ReadDataAdminContainer();
        }

        async void ReadDataUrlContainer()
        {
            Debug.WriteLine("Read Data Url Container");
            await ReadDataUrl();
        }

        async void ReadDataAdminContainer()
        {
            Debug.WriteLine("Read Data Admin Container");
            await ReadDataAdmin();
        }

        Windows.Storage.StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;

        async Task ReadDataUrl()
        {
            try
            {
                Debug.WriteLine("Read Data Url");

                StorageFile File = await localFolder.GetFileAsync("shopurl.conf");
                String ReadString = await FileIO.ReadTextAsync(File);
                ShopUrlTwo.Text = ReadString;

                Global.URL = Global.HTTPS + ShopUrlTwo.Text + "/admin";
                Uri uri = new Uri(Global.URL);
                // webView.Navigate(uri); // lets not make webview navigate yet

            }
            catch (FileNotFoundException e)
            {
                // Cannot find file
            }
            catch (IOException e)
            {
                // Get information from the exception, then throw
                // the info to the parent method.
                if (e.Source != null)
                {
                    Debug.WriteLine("IOException source: {0}", e.Source);
                }
                throw;
            }
        }


        async Task ReadDataAdmin()
        {
            try
            {
                Debug.WriteLine("Read Data Admin");
                StorageFile FileAdmin = await localFolder.GetFileAsync("shopadmin.conf");
                String ReadStringAdmin = await FileIO.ReadTextAsync(FileAdmin);

                Global.ADMIN = ReadStringAdmin;

                Global.URL = Global.HTTPS + ShopUrlTwo.Text + "/admin" + ReadStringAdmin;
                Uri uri = new Uri(Global.URL);
                webView.Navigate(uri);
                LoadingStart();
            }
            catch (FileNotFoundException e)
            {
                // Cannot find file
            }
            catch (IOException e)
            {
                // Get information from the exception, then throw
                // the info to the parent method.
                if (e.Source != null)
                {
                    Debug.WriteLine("IOException source: {0}", e.Source);
                }
                throw;
            }
        }




        private void panel_load(object sender, RoutedEventArgs e)
        {
            ShopUrlTwo.Focus(FocusState.Programmatic);
        }

        private void Url_Go_Click(object sender, RoutedEventArgs e)
        {

            Global.URL = Global.HTTPS + ShopUrlTwo.Text;
            
                Uri uri = new Uri(Global.URL + "/admin");
                webView.Navigate(uri);
               
        }

        private void Home(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            string ShopUrlVarNew = Global.URL;
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            HomeBtn.Background = new SolidColorBrush(Windows.UI.Colors.White);

            //HomeBtn.ClearValue(Button.BackgroundProperty);
            //HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);

        }

        private void Orders(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            string ShopUrlVarNew = Global.URL + "/orders";
            Global.ADMIN = "/orders";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            orders.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            //orders.ClearValue(Button.BackgroundProperty);
            //orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);



            ShopUrlVarNew = ShopUrlVarNew.Replace("/orders", "");
        }
        private void Products(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            string ShopUrlVarNew = Global.URL + "/products";
            Global.ADMIN = "/products";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            products.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            //products.ClearValue(Button.BackgroundProperty);
            //products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);

            ShopUrlVarNew = ShopUrlVarNew.Replace("/products", "");
        }
        private void Collections(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            string ShopUrlVarNew = Global.URL + "/collections";
            Global.ADMIN = "/collections";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            collections.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            //collections.ClearValue(Button.BackgroundProperty);
            //collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);

            ShopUrlVarNew = ShopUrlVarNew.Replace("/collections", "");
        }
        private void Customers(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            var ShopUrlVarNew = Global.URL + "/customers";
            Global.ADMIN = "/customers";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            customers.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            //customers.ClearValue(Button.BackgroundProperty);
            //customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);

            ShopUrlVarNew = ShopUrlVarNew.Replace("/customers", "");
        }
        private void Analytics(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            var ShopUrlVarNew = Global.URL + "/dashboards";
            Global.ADMIN = "/dashboards";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            analytics.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            //analytics.ClearValue(Button.BackgroundProperty);
            //analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);

            ShopUrlVarNew = ShopUrlVarNew.Replace("/dashboards", "");
        }
        private void Marketing(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            var ShopUrlVarNew = Global.URL + "/marketing";
            Global.ADMIN = "/marketing";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            marketing.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            //marketing.ClearValue(Button.BackgroundProperty);
            //marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);

            ShopUrlVarNew = ShopUrlVarNew.Replace("/marketing", "");
        }
        private void Discounts(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            var ShopUrlVarNew = Global.URL + "/discounts";
            Global.ADMIN = "/discounts";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            discounts.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            //discounts.ClearValue(Button.BackgroundProperty);
            //discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);

            ShopUrlVarNew = ShopUrlVarNew.Replace("/discounts", "");
        }
        private void Apps(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            var ShopUrlVarNew = Global.URL + "/apps";
            Global.ADMIN = "/apps";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            apps.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            //apps.ClearValue(Button.BackgroundProperty);
            //apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);

            ShopUrlVarNew = ShopUrlVarNew.Replace("/apps", "");
        }
        private void Themes(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            var ShopUrlVarNew = Global.URL + "/themes";
            Global.ADMIN = "/themes";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            themes.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            //themes.ClearValue(Button.BackgroundProperty);
            //themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);

            ShopUrlVarNew = ShopUrlVarNew.Replace("/themes", "");
        }
        private void BlogPosts(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            var ShopUrlVarNew = Global.URL + "/articles";
            Global.ADMIN = "/articles";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            blogposts.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            //blogposts.ClearValue(Button.BackgroundProperty);
            //blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);

            ShopUrlVarNew = ShopUrlVarNew.Replace("/articles", "");
        }
        private void Pages(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            var ShopUrlVarNew = Global.URL + "/pages";
            Global.ADMIN = "/pages";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            pages.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            //pages.ClearValue(Button.BackgroundProperty);
            //pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);

            ShopUrlVarNew = ShopUrlVarNew.Replace("/pages", "");
        }
        private void Navigation(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            var ShopUrlVarNew = Global.URL + "/menus";
            Global.ADMIN = "/menus";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            navigiation.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            //navigiation.ClearValue(Button.BackgroundProperty);
            //navigiation.ClearValue(Button.ForegroundProperty);
            domains.ClearValue(Button.BackgroundProperty);
            domains.ClearValue(Button.ForegroundProperty);

            ShopUrlVarNew = ShopUrlVarNew.Replace("/menus", "");
        }
        private void Domains(object sender, RoutedEventArgs e)
        {
            webView.Visibility = Visibility.Collapsed;
            LoadingStart();

            CheckGlobalUrl();

            var ShopUrlVarNew = Global.URL + "/settings/domains";
            Global.ADMIN = "/settings/domains";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            domains.Background = new SolidColorBrush(Windows.UI.Colors.White);

            HomeBtn.ClearValue(Button.BackgroundProperty);
            HomeBtn.ClearValue(Button.ForegroundProperty);
            orders.ClearValue(Button.BackgroundProperty);
            orders.ClearValue(Button.ForegroundProperty);
            products.ClearValue(Button.BackgroundProperty);
            products.ClearValue(Button.ForegroundProperty);
            collections.ClearValue(Button.BackgroundProperty);
            collections.ClearValue(Button.ForegroundProperty);
            customers.ClearValue(Button.BackgroundProperty);
            customers.ClearValue(Button.ForegroundProperty);
            analytics.ClearValue(Button.BackgroundProperty);
            analytics.ClearValue(Button.ForegroundProperty);
            marketing.ClearValue(Button.BackgroundProperty);
            marketing.ClearValue(Button.ForegroundProperty);
            discounts.ClearValue(Button.BackgroundProperty);
            discounts.ClearValue(Button.ForegroundProperty);
            apps.ClearValue(Button.BackgroundProperty);
            apps.ClearValue(Button.ForegroundProperty);
            themes.ClearValue(Button.BackgroundProperty);
            themes.ClearValue(Button.ForegroundProperty);
            blogposts.ClearValue(Button.BackgroundProperty);
            blogposts.ClearValue(Button.ForegroundProperty);
            pages.ClearValue(Button.BackgroundProperty);
            pages.ClearValue(Button.ForegroundProperty);
            navigiation.ClearValue(Button.BackgroundProperty);
            navigiation.ClearValue(Button.ForegroundProperty);
            //domains.ClearValue(Button.BackgroundProperty);
            //domains.ClearValue(Button.ForegroundProperty);

            ShopUrlVarNew = ShopUrlVarNew.Replace("/settings/domains", "");
        }
        private void Locations(object sender, RoutedEventArgs e) //this is not in xaml
        {

            CheckGlobalUrl();

            var ShopUrlVarNew = Global.URL + "/settings/locations";
            Global.ADMIN = "/settings/locations";
            Uri uri = new Uri(ShopUrlVarNew);
            webView.Navigate(uri);
            ShopUrlVarNew = ShopUrlVarNew.Replace("/settings/locations", "");
        }

        private void ShopUrl_TextChanged(object sender, TextChangedEventArgs e)
        {

        Global.URL = Global.HTTPS + ShopUrlTwo.Text + "/admin";                
                   
        if (Uri.IsWellFormedUriString(Global.URL, UriKind.Absolute))
            {
            Uri uri = new Uri(Global.URL);
            webView.Navigate(uri);
            }
        }

        private void webView_LoadCompleted(object sender, NavigationEventArgs e)
        {
            WriteDataUrl();
            WriteDataAdmin();

            String CheckAA = e.Uri.ToString();
            String CheckBB = "themes/";

            Debug.WriteLine(CheckAA + CheckBB);


            String uCheck = ShopUrlTwo.Text;
            if (uCheck != "myshopify.com")
            {
                ShopUrlTwo.SelectionStart = ShopUrlTwo.Text.Length;
                ShopUrlTwo.SelectionLength = 0;
            }

            if (CheckAA.Contains(CheckBB))
            {
                //the theme may be being edited
            }


            String StartUpButtonColor = "";
            Global.ADMIN = StartUpButtonColor;

            if (CheckAA.Contains("/orders"))
            {
                orders.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
            else if (CheckAA.Contains("/products"))
            {
                products.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
            else if (CheckAA.Contains("/collections"))
            {
                collections.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
            else if (CheckAA.Contains("/customers"))
            {
                customers.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
            else if (CheckAA.Contains("/dashboards"))
            {
                analytics.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
            else if (CheckAA.Contains("/marketing"))
            {
                marketing.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
            else if (CheckAA.Contains("/discounts"))
            {
                discounts.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
            else if (CheckAA.Contains("/apps"))
            {
                apps.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
            else if (CheckAA.Contains("/themes"))
            {
                themes.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
            else if (CheckAA.Contains("/articles"))
            {
                blogposts.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
            else if (CheckAA.Contains("/pages"))
            {
                pages.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
            else if (CheckAA.Contains("/menus"))
            {
                navigiation.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
            else
            {
                HomeBtn.Background = new SolidColorBrush(Windows.UI.Colors.White);
            }
        }

        private async void WebView_OnNavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {



            webView.Visibility = Visibility.Visible;
            LoadingStop();


            Debug.WriteLine("dd");

            try
            {
                await this.webView.InvokeScriptAsync("eval", new[]
            {


                "document.getElementById(\"AppFrameTopBar\").style.visibility = \"hidden\";"
              });
                 }
             catch { }

            try { 
            await this.webView.InvokeScriptAsync("eval", new[]
            {


                "document.getElementById(\"AppFrameNav\").style.visibility = \"hidden\";"
               });
                }
             catch { }

            try
            {
                await this.webView.InvokeScriptAsync("eval", new[]
            {


                "document.getElementById(\"AppFrameLoadingBar\").style.visibility = \"hidden\";"
              });
            }
            catch { }


            try {
                await this.webView.InvokeScriptAsync("eval", new[]
                {


                "document.getElementById(\"AppFrameMain\").style.padding = \"0\";"
                });
            }
           catch { }

               
            } 
        
    


        private void WebView_OnScriptNotify(object sender, NotifyEventArgs e)
        {
            var url = e.Value;
            Debug.WriteLine("the script has been triggered");
        }

        private void CheckGlobalUrl()
        {
            string stringToCheck = Global.URL;
            string[] stringArray = { "/orders", "/products", "/collections", "/customers", "/dashboards", "/marketing", "/discounts", "/apps", "/themes", "/articles", "/pages", "/menus", "/settings/domains", "/settings/locations" };
            if (stringArray.Any(stringToCheck.Contains))
            {
                Global.URL = Global.URL.Replace("/orders", "")
                                       .Replace("/products", "")
                                       .Replace("/collections", "")
                                       .Replace("/customers", "")
                                       .Replace("/dashboards", "")
                                       .Replace("/marketing", "")
                                       .Replace("/discounts", "")
                                       .Replace("/apps", "")
                                       .Replace("/themes", "")
                                       .Replace("/articles", "")
                                       .Replace("/pages", "")
                                       .Replace("/menus", "")
                                       .Replace("/settings/domains", "")
                                       .Replace("/settings/locations", "");
            }
            else
            {
                return;
            }
        }

        async void WriteDataUrl()
        {
            string WriteString = ShopUrlTwo.Text;
            StorageFile File = await localFolder.CreateFileAsync("shopurl.conf", CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(File, WriteString);
        }

        async void WriteDataAdmin()
        {

            if (Global.ADMIN == null)
            {
                // string is null do not write
            }
            else
            {
                string WriteString = Global.ADMIN;
                StorageFile File = await localFolder.CreateFileAsync("shopadmin.conf", CreationCollisionOption.ReplaceExisting);
                await FileIO.WriteTextAsync(File, WriteString);
            }
        }

        private void LoadingStart()
        {
           
                    progress.IsActive = true;
                    progress.Visibility = Visibility.Visible;
                }
        private void LoadingStop()
        {
                    progress.IsActive = false;
                    progress.Visibility = Visibility.Collapsed;
                }
    }
}
    


public static class Global
{
    public static string HTTPS;
    public static string URL;
    public static string ADMIN;
}